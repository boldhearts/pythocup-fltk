#! /usr/bin/python 

# pythocup: a strongly simplified simulator for the 2D RoboCup simulation.

# Copyright (C) 2013

# Author: Daniel Polani <d.polani@herts.ac.uk>

# pythocup is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from fltk import *
from math import *
import sys

# the following is only needed for debugging
#from debug_fltk_constants import *

# physical parameters

dt = 0.02
velocity_decay = pow(0.8, dt)          # 0.95 per second
kick_dist = 2
coll_dist = 1

# visualization parameters

pixel_per_meter = 790/100
ppm = pixel_per_meter
window_size_x = 800
window_size_y = 500
offset_x = 0
offset_y = 0
delta_offset = 12

field_length = 100
field_width = 60

def cup_shifted_line(x1, y1, x2, y2):
    fl_line(offset_x + int(ppm * x1),
            offset_y + int(ppm * y1),
            offset_x + int(ppm * x2),
            offset_y + int(ppm * y2))

def cup_shifted_arc(x1, y1, w, h, a1, a2):
    fl_arc(offset_x + int(ppm * x1),
           offset_y + int(ppm * y1),
           int(ppm * w),
           int(ppm * h),
           0,360)
    

# naming parameters

other_side = { "left" : "right",
               "right": "left" }
goal_pos = { "left" : (0, field_width/2.0),
             "right": (field_length, field_width/2.0) }

def hard_limit(x, min_x, max_x):
    return max(min(x, max_x), min_x)

def move_limit(move, max_speed):
    #return tuple(hard_limit(m,  for m in move) # hard_limit(move[0]), hard_limit(move[1])
    speed = vector_length(move)

    # limit the move velocity
    if speed > max_speed:
        #print "maximum velocity exceeded, clipped to:", tuple(m/float(speed)*max_speed for m in move)
        return tuple(m/float(speed)*max_speed for m in move)
    else:
        return move
 

def vector_length(v):
    assert len(v) == 2
    return sqrt(v[0]*v[0] + v[1]*v[1])

def dist(x, y):
    return vector_length((y[0]-x[0], y[1]-x[1]))

def scalmult(k, v):
    return (k*v[0], k*v[1])

def normalize(v):
    norm = vector_length(v)
    # if not norm: return 0,0
    return scalmult(1.0/norm, v)

def vector_sub(v, u):
    return v[0]-u[0], v[1]-u[1]

class CupObject:
    def __init__(self, world, name = ""):
        self.world = world              # pointer back to world for various operations
        self.name = name
        self.p = (0,0)
        self.__sorted_object = None
        self.__sorting_valid = False

    def sorted_object(self, i = None):
        """Return the i-th object, sorted by distance from me. The 0-th
        object is myself. Also works if I am the ball.

        With no argument, return length of list."""

        if not self.__sorted_object:
            # it has not been initialized, set it
            self.__sorted_object = list(self.world.objectlist)
            self.__sorting_valid = False
        # I think that the list contents, once initialized, should
        # never change, but just to make sure
        assert len(self.__sorted_object) == len(self.world.objectlist)

        # the list is now set, but not necessarily sorted yet

        if not i:
            # we are interested only in the length, don't bother
            # sorting yet. It is likely that we may need to sort
            # later, but that can happen on demand.
            return len(self.__sorted_object)

        if not self.__sorting_valid:    # need to resort?
            self.__sorted_object.sort(key = lambda agent: dist(agent.p, self.p))
            # relies on agent being a pointer to the original agents
            self.__sorting_valid = True
        
        return self.__sorted_object[i] # return the agent straight

    def step(self):
        global dt
        self.__sorting_valid = False    # stepping invalidates all distances

        # here we rely on step being called for every agent, since if
        # only part of agents would be stepped, the others would think
        # that their distances are still valid. This is not a safe
        # solution, but short of writing a centralized solution for
        # maintaining distances (and there we end up with the problem
        # of maintaining agent identities which introduces another
        # ballpark of difficulties), that's the best that I can do for
        # a Q&D solution.

        self.p = (self.p[0] + self.v[0] * dt,
                  self.p[1] + self.v[1] * dt)


    def position(self, p = None):
        if not p:
            return self.p
        else:
            self.p = p

    def velocity(self, v = None):
        if not v:
            return self.v
        else:
            self.v = v
    def x(self):
        return self.p[0]
    def y(self):
        return self.p[1]

class Goal(CupObject):                  # everything's an object
    global field_length, field_width, goal_pos

    def __init__(self, world, side):
        CupObject.__init__(self, world)
        self.v = 0,0
        self.p = goal_pos[side]
        self.h = 12.5
        # inconsistent notation, here we use the graphical FLTK
        # convention, of h in graphical coordinates, instead of goal
        # width. I have no time for cleanup now

        self.side = side
        # required by world - I am not sure that treating a goal like
        # an object is such a good idea after all. We'll see.

    def __repr__(self):
        return "Goal: " + str((self.p, self.v))

    def draw(self):
        global ppm
        fl_color(255,255,255)
        cup_shifted_line(self.x(),
                         self.y() - self.h/2.0,
                         self.x(),
                         self.y() + self.h/2.0)

class Ball(CupObject):
    def __init__(self, world):
        CupObject.__init__(self, world)
        self.v = 0,0
        self.w = 1.5
        self.h = 1.5

    def __repr__(self):
        return "Ball: " + str((self.p, self.v))

    def step(self):
        old_p = self.p # store old position for comparison with goal
        CupObject.step(self)                  # object-independent dynamics
        self.world.detect_goal(old_p, self.p) # was there a goal between the
                                        # two new positions?

        self.v = (self.v[0] * velocity_decay, # object-specific dynamics
                  self.v[1] * velocity_decay)

        for i in range(1, self.sorted_object()): 
            # for every object it collides with
            coll_object = self.sorted_object(i)

            if dist(coll_object.p, self.p) <= coll_dist:
                if coll_object.__class__ == Goal:
                    continue            # a goal does not collide
                # debug
                #print "Ball collides at location:", self.p

                # move away from object, the ball is much lighter
                correction = scalmult(coll_dist,
                                      normalize((self.x() - coll_object.x(),
                                                 self.y() - coll_object.y())))

                old_p = self.p
                self.p = self.p[0] + correction[0], self.p[1] + correction[1]
                self.world.detect_goal(old_p, self.p) # the ball was moved, was there a goal?

                self.v = scalmult(-0.65, self.v) # turn direction and dampen
            else:
                break                   # stop when the first non-collision object appears
                
        

    def kick(self, k):                  # when it's kicked
        self.v = (self.v[0] + dt * k[0],
                  self.v[1] + dt * k[1])

    def reset(self):
        self.p = (field_length/2.0,field_width/2.0)
        self.v = (0,0)

    def draw(self):
        global ppm
        fl_color(255,255,255)
        cup_shifted_arc(self.x()-self.w/2.0,
                        self.y()-self.h/2.0,
                        self.w,
                        self.h,
                        0,360)

class Agent(CupObject):
    def __init__(self, name, world, brain):
        CupObject.__init__(self, world, name)
        self.v = 0,0
        self.w = 2
        self.h = 2

        self.max_speed = 8                  # maximum speed
        self.max_kick_speed = 30

        #self.world = world
        self.brain = brain
        self.brain._world(world)
        self.brain._agent(self)

    def __repr__(self):
        return "Agent: " + str((self.p, self.v))

    def step(self):
        CupObject.step(self)          # instance-independent dynamics
        move, kick = self.brain.action()   # move is the new velocity, kick the kick strength

        self.v = move_limit(move, self.max_speed) # need to store in v
        kick = move_limit(kick, self.max_kick_speed) # no storage needed

        # kick only works in kick distance
        if dist(self.world.ball.p, self.p) <= kick_dist:
            self.world.ball.kick(kick)

        for i in range(1, self.sorted_object()): 
            # for every object it collides with

            coll_object = self.sorted_object(i)

            # if it's a goal, ignore
            if coll_object.__class__ == Goal:
                continue
            # now check whether it's a ball or an agent, agents are bigger
            elif coll_object.__class__ == Agent:
                coll_factor = 2
            elif coll_object.__class__ == Ball:
                coll_factor = 1

            if dist(self.sorted_object(i).p, self.p) <= coll_dist * coll_factor:
                # move away from object, the ball is much lighter
                correction = scalmult(0.5 * coll_dist * coll_factor,
                                      normalize((self.x() - self.sorted_object(i).x(),
                                                 self.y() - self.sorted_object(i).y())))

                self.p = self.p[0] + correction[0], self.p[1] + correction[1]
                self.v = (0,0)          # and stop
            else:
                break                   # stop when the first non-collision object appears


    def draw(self):
        global ppm
        if self.brain.side == "left":
            fl_color(255,0,0)
        else:
            fl_color(255,255,0)

        cup_shifted_arc(self.x()-self.w/2.0,
                        self.y()-self.h/2.0,
                        self.w,
                        self.h,
                        0,360)

        #world.kick(kick)

class Brain:
    def __init__(self, side = "left"):
        self.__world = None                    # needs initialization
        self.side = side
    
    # you do not need to actually access these in the derived classes
    def _world(self, world = None):
        if world:
            self.__world = world
        else:
            return self.__world

    def _agent(self, agent = None):      # pointer to the current agent
        if agent:
            self.__agent = agent
        else:
            return self.__agent

    # only these need to be accessed by the derived classes
    def ball(self):
        return self._world().ball

    def x(self):
        return self._agent().x()                # filtered by sensors?

    def y(self):
        return self._agent().y()

    def p(self):                        # if you want both coordinates
        return self._agent().p

    def v(self):
        return self._agent().v

    def sorted_object(self, i = None):
        return self._agent().sorted_object(i)

class Franken_Brain(Brain):
    # if you want to set the side
    #def __init__(self, side = None):
    #    Brain.__init__(self, side)

    def action(self):
        import random
        return (random.random() * 20 - 10,
                random.random() * 20 - 10),\
                (0,0)

class Rush_To_Brain(Brain):
    def action(self):
        # where is the ball

        ball = self.ball()

        v = (ball.x() - self.x(), ball.y() - self.y())
        v = (v[0]/vector_length(v) * 1000,
             v[1]/vector_length(v) * 1000)
        #print "v", v
        return v, (0,0)                 # no kick

class Rush_To_Other_Agent(Brain):
    def action(self):
        for i in range(1, self.sorted_object()):
            # dummy loop, really should not break, though, as long as
            # there is something else than me in the world

            # this one does the same as the Rushing to Ball agent
            if self.sorted_object(i).__class__ == Agent: # Ball
                target = self.sorted_object(i)

                v = (target.x() - self.x(), target.y() - self.y())
                vl = vector_length(v)   # make sure it's not 0

                if not vl:
                    v = (0,0)
                else:
                    v = (v[0]/vector_length(v) * 1000,
                         v[1]/vector_length(v) * 1000)

                return v, (0,0)
            

class Kick_To_Goal_Agent(Brain):
    def action(self):
        for i in range(1, self.sorted_object()):
            # dummy loop, really should not break, though, as long as
            # there is something else than me in the world

            # this one does the same as the Rushing to Ball agent
            if self.sorted_object(i).__class__ == Ball:
                #print "Kick"
                target = self.sorted_object(i)

                # go to ball
                v = (target.x() - self.x(), target.y() - self.y())
                vl = vector_length(v)   # make sure it's not 0

                if not vl:
                    v = (0,0)
                else:
                    v = (v[0]/vector_length(v) * 1000,
                         v[1]/vector_length(v) * 1000)

                k = (goal_pos[other_side[self.side]][0] - self.ball().x(),
                     goal_pos[other_side[self.side]][1] - self.ball().y())

                #print "k", k

                ## print self.side
                ## print other_side[self.side]
                ## print goal_pos[other_side[self.side]]
                ## print self.ball().x()

                ## print "Kick: ", scalmult(100, k)

                return v, scalmult(100, k)
            



class CupObjectWidget(Fl_Widget):
    def __init__(self,x,y,w,h,world):
        Fl_Widget.__init__(self, 0, 0, w, h, "PythoCup")
        #self._image=image
        #self.posx, self.posy = 0,0
        self.world = world

    def draw(self):
        #w,h=self.w(),self.h() 
        #fl_draw_image(self._image,w/2-50,h/2-50,width,height,4,0)
        #self.cupobject.draw()
        for o in self.world.objects():
            #print "CupObjectWidget", o 
            o.draw()

        fl_color(255,255,155)
        fl_font(FL_TIMES, 50)
        fl_draw(str(self.world.score["left"]), 
                20, window_size_y - 20 - fl_descent()) # align to the bottom

        ## fl_draw(str(self.world.score["left"]),
        ##         10, window_size_y - 10, 30, 60, FL_ALIGN_TOP)

        # aligning the right one is slightly more work
        right_score = str(self.world.score["right"])
        fl_draw(right_score,
                window_size_x - 20 - fl_measure(right_score)[0],
                # width of string reduced
                window_size_y - 20 - fl_descent())
        
    def handle(self, event):
        global offset_x, offset_y
        #print "some event", event
        #print event, fltk_constant_names[event]

        if event == FL_KEYUP:
            #print fltk_constant_names[Fl.event_key()]
            if Fl.event_key() == FL_Right:
                #print "right"
                offset_x -= delta_offset
                return 1
            elif Fl.event_key() == FL_Left:
                #print "left"
                offset_x += delta_offset
                return 1
            elif Fl.event_key() == FL_Up:
                #print "up"
                offset_y += delta_offset
                return 1
            elif Fl.event_key() == FL_Down:
                #print "down"
                offset_y -= delta_offset
                return 1
        return 0

class World:
    def __init__(self):
        self.objectlist = []
        self.goal = {}
        self.score = { "left": 0,
                       "right":0 }

    def __repr__(self):
        return " ".join(str(o) for o in self.objects())

    def add(self, ob):
        #print ob
        #print ob.__class__
        self.objectlist.append(ob)

        # special object pointer for ball

        if ob.__class__ == Ball:
            #print "added ball"
            self.ball = ob
        elif ob.__class__ == Goal:
            self.goal[ob.side] = ob     # store the goals for later use

    def objects(self):
        for o in self.objectlist:
            yield o

    def step(self):
        #assert type(self.objects()[0]) == Ball # the first must be the ball

        for o in self.objects():
            o.step()

    def detect_goal(self, old_p, new_p):
        dx = new_p[0] - old_p[0], new_p[1] - old_p[1]
        dg = 0, self.goal["left"].h     # is the same for both goals dg = g_2 - g_1
        # for u, we need two goals for testing. In addition, we need
        # to make sure that the orientation is right, from the start
        # corner to the end corner
        ## u = { "left": vector_sub(self.goal["left"].p, old_p), # g - b
        ##       "right":vector_sub(self.goal["right"].p, old_p) } # for the right goal
        for side in ["left", "right"]:
            # could have used self.goal.keys(), but this is more readable

            det = dx[0]*dg[1] - dx[1]*dg[0] # determinant of the
                                            # linear system

            # if the determinant is 0, the ball step and the lines do
            # not cross, no goal
            if not det:
                continue                # try the next goal

            # at this point, the current goal is still in the running
            
            # compute u = g - b
            u = vector_sub(self.goal[side].p, old_p) # g - b

            t = (u[0]*dg[1] - u[1]*dg[0])/det

            if t < 0 or t > 1:
                # the intersection is outside of the interval [0,1],
                # and thus outside of the current boundaries of the
                # step. No goal.
                continue                # and try next one

            # at this point, we confirmed that current goal line was
            # intersected. We still need to test whether the ball
            # actually moves in the right direction. We do not care
            # about a ball that moves in from the goal into the field.

            if side == "left":
                # ball must move from inside to outside of field for
                # the goal to count

                if dx[0] >= 0:
                    # it is moving from outside to inside the field,
                    # this is certainly no goal. However, since we
                    # crossed the present goal line, there is no
                    # necessity to check the other goal - yes, I know:
                    # if the ball does superluminal speed, still the
                    # other side can catch a goal, but I will not
                    # assume this to save a bit of time.
                    return              # without doing anything

                # yes, it enters the goal from the right side, it
                # counts
                
                self.score["right"] += 1
                self.ball.reset()
                return                  # done
            if side == "right":
                # analogous to above
                if dx[0] <= 0:
                    # moving from right to left (or on the line)
                    return              # no goal

                self.score["left"] += 1
                self.ball.reset()
                return
    

def populate_world(world):
    
    # always start with the ball
    ## ball = Ball(world)
    ## ball.position((50,30))
    ## ball.velocity((10,3))

    ## world.add(ball)

    ## world.add(Goal(world, "left"))
    ## world.add(Goal(world, "right"))

    ## brain = Franken_Brain()
    ## agent = Agent("Cazoomba", world, brain)
    ## agent.position((40,40))
    ## world.add(agent)

    ## brain = Rush_To_Brain()
    ## agent = Agent("Brainzomba", world, brain)
    ## agent.position((80,10))
    ## world.add(agent)

    ## brain = Rush_To_Other_Agent()
    ## agent = Agent("Other Agent", world, brain)
    ## agent.position((20,30))
    ## world.add(agent)

    ## brain = Rush_To_Other_Agent("right")
    ## agent = Agent("Other Agent", world, brain)
    ## agent.position((30,10))
    ## world.add(agent)

    brain = Kick_To_Goal_Agent("right")
    agent = Agent("Other Agent", world, brain)
    agent.position((30,10))
    world.add(agent)

    brain = Kick_To_Goal_Agent("right")
    agent = Agent("Other Agent", world, brain)
    agent.position((90,40))
    world.add(agent)


    brain = Kick_To_Goal_Agent("left")
    agent = Agent("One Agent", world, brain)
    agent.position((20,20))
    world.add(agent)

    return world

########################################################################

def next_image():
    Fl_repeat_timeout(dt, next_image)

def generate_window(world):
    global CupObjectWidget
    window = Fl_Double_Window(window_size_x,window_size_y, "PythoCup")
    window.color(FL_BLACK)

    widget = CupObjectWidget(0,0,window.w(),window.h(),world)
    # make sure widget is returned or its life ends in this function, but it should be only killed by the window

    window.resizable(window)
    window.end()
    window.show(len(sys.argv), sys.argv)
    return window, widget

########################################################################

class PythoCup:
    def __init__(self, callback = None):
        self.world = self.prepopulate(World())
        self.world = self.populate_world(self.world)
        self.window, self.widget = generate_window(self.world) # must return the widget.
        Fl_add_timeout(dt, next_image)

    #def populate_world(self, world):    # to be overwritten
    #populate_world(world)

    def prepopulate(self, world):
        ball = Ball(world)
        ball.position((50,30))
        ball.velocity((10,3))
        world.add(ball)

        world.add(Goal(world, "left"))
        world.add(Goal(world, "right"))
        
        return world

    def run(self):
        #Fl.run()
        #print self.world
        while Fl.wait() > 0:
            self.world.step()
            self.window.redraw()

class Sample_Sim(PythoCup):
    def populate_world(self, world):
        return populate_world(world)
