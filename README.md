# PythoCup

PythoCup is a python-based simple simulator to study several aspects of the 2D RoboCup scenario without the hassle of understanding the detailed synchronization and command model of the standard 2D simulator. It simplifies many aspects, does not include turns, currently does not implement noise, outs (field boundaries) and many other features, and has been developed on-the-fly for the purpose of an quick-access to 2D RoboCup scenarios.

## Prerequirements
PythoCup has been developed for Python 2 under Linux and requires the FLTK 1.3 toolkit, together with the pyFLTK 1.3 python bindings. Any system which runs the appropriate FLTK version and the bindings should be able to run PythoCup. 

fOn Ubuntu, e..g. 16.04, , packages `libfltk1.3-dev` `python-fltk` were provided:

	apt install libfltk1.3-dev python-fltk

*(There is no python wrapper (i.e. python-fltk) provided on for Ubuntu >= 16.04)*

## Files

- *pythocup.py*: the main simulator. It contains the core operation of the simulator in one file.
- *debug_fltk_constants.py*: not necessary in normal operation. Helps debugging the values of various FLTK constants under python binding.
- *sample.py*: simple file running a standard scenario built into the simulator, showing the basic use.
- *trivial_agent.py*: basic scenario from which more elaborate agents can be constructed

## Simple Demo

This is the content of *simple.py*:

```
from pythocup import *

sim = Sample_Sim()
sim.run()
```

The first line imports all necessary components. `Sample_Sim()` constructs a simulation with a sample set of agents and setup. 

`sim.run()` then runs the event loop. By inspection of the event loop in `pythocup.py` one can modify the behaviour, but we recommend initialy to operate with the original event loop. 

## Further Information

You can read about [basic concepts](https://gitlab.com/boldhearts/pythocup/wikis/Basic-Concepts) and used [classes and helpfer functions](https://gitlab.com/boldhearts/pythocup/wikis/Classes-and-helpfer-functions) in the wiki. There is also a set of [exercises](https://gitlab.com/boldhearts/pythocup/wikis/Exercises) to begin with.

# Contribution
You're welcome to contribute. Please have a look at [Gitlab's workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html) first. It is helpful if each commit addresses a specific feature before requesting a merge. You can achieve this with, e.g., squashing the commits of your working copy.

