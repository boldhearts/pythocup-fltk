#! /usr/bin/python 

# trivial_agent: Demo for setting up a simple agent in pythocup

# Copyright (C) 2013

# Author: Daniel Polani <d.polani@herts.ac.uk>

# trivial_agent is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from pythocup import *

class Trivial_Agent(Brain):
    def action(self):
        # return a double tuple v_agent, v_ball, each of them of the
        # form (vx, vy)

        # the agent velocity is immediately assumed within the allowed
        # limits, the ball velocity specifies the desired value.

        # if I wanted, I can inspect the objects of interests, sorted
        # by distance to me. The i-th object is self.sorted_object(i),
        # the length of this list is self.sorted_object()

        if vector_length(self.v()) < 5:
            # if too slow, move, without kicking
            v = (8, 5)
            return v, (0,0)

        v = (self.v()[0]-0.05*self.v()[1],
             self.v()[1]+0.05*self.v()[0])
        k = 100, 20

        return v, k
        

class Trivial_Sim(PythoCup):
    def populate_world(self, world):
        brain = Trivial_Agent("left")
        agent = Agent("Agent without a name", world, brain)
        agent.position((30,10))         # you can set it if you like
        world.add(agent)

        return world                    # don't forget

sim = Trivial_Sim()

sim.run()
