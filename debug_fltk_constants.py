#! /usr/bin/python 

# debug_fltk_constants.py: utility to debug fltk constants in Python.

# Copyright (C) 2013

# Author: Daniel Polani <d.polani@herts.ac.uk>

# debug_fltk_constants.py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from fltk import *

# identify constants

fltk_constant_names = { FL_NO_EVENT: "FL_NO_EVENT", 
                        FL_PUSH: "FL_PUSH", 
                        FL_RELEASE: "FL_RELEASE", 
                        FL_ENTER: "FL_ENTER", 
                        FL_LEAVE: "FL_LEAVE", 
                        FL_DRAG: "FL_DRAG", 
                        FL_FOCUS: "FL_FOCUS", 
                        FL_UNFOCUS: "FL_UNFOCUS", 
                        FL_KEYBOARD: "FL_KEYBOARD", 
                        FL_CLOSE: "FL_CLOSE", 
                        FL_MOVE: "FL_MOVE", 
                        FL_SHORTCUT: "FL_SHORTCUT", 
                        FL_DEACTIVATE: "FL_DEACTIVATE", 
                        FL_ACTIVATE: "FL_ACTIVATE", 
                        FL_HIDE: "FL_HIDE", 
                        FL_SHOW: "FL_SHOW", 
                        FL_PASTE: "FL_PASTE", 
                        FL_SELECTIONCLEAR: "FL_SELECTIONCLEAR", 
                        FL_MOUSEWHEEL: "FL_MOUSEWHEEL", 
                        FL_DND_ENTER: "FL_DND_ENTER", 
                        FL_DND_DRAG: "FL_DND_DRAG", 
                        FL_DND_LEAVE: "FL_DND_LEAVE", 
                        FL_DND_RELEASE: "FL_DND_RELEASE",
                        FL_KEYUP: "FL_KEYUP",
                        FL_LEFT_MOUSE: "FL_LEFT_MOUSE", 
                        FL_MIDDLE_MOUSE: "FL_MIDDLE_MOUSE", 
                        FL_RIGHT_MOUSE: "FL_RIGHT_MOUSE",
                        FL_Button: "FL_Button", 
                        FL_BackSpace: "FL_BackSpace", 
                        FL_Tab: "FL_Tab", 
                        FL_Enter: "FL_Enter", 
                        FL_Pause: "FL_Pause", 
                        FL_Scroll_Lock: "FL_Scroll_Lock", 
                        FL_Escape: "FL_Escape", 
                        FL_Home: "FL_Home", 
                        FL_Left: "FL_Left", 
                        FL_Up: "FL_Up", 
                        FL_Right: "FL_Right", 
                        FL_Down: "FL_Down", 
                        FL_Page_Up: "FL_Page_Up", 
                        FL_Page_Down: "FL_Page_Down", 
                        FL_End: "FL_End", 
                        FL_Print: "FL_Print", 
                        FL_Insert: "FL_Insert", 
                        FL_Menu: "FL_Menu", 
                        FL_Num_Lock: "FL_Num_Lock", 
                        FL_KP: "FL_KP", 
                        FL_KP_Enter: "FL_KP_Enter", 
                        FL_F: "FL_F", 
                        FL_Shift_L: "FL_Shift_L", 
                        FL_Shift_R: "FL_Shift_R", 
                        FL_Control_L: "FL_Control_L", 
                        FL_Control_R: "FL_Control_R", 
                        FL_Caps_Lock: "FL_Caps_Lock", 
                        FL_Meta_L: "FL_Meta_L", 
                        FL_Meta_R: "FL_Meta_R", 
                        FL_Alt_L: "FL_Alt_L", 
                        FL_Alt_R: "FL_Alt_R", 
                        FL_Delete: "FL_Delete" }


